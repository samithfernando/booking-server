import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../environments/environment'
import { CookieService } from 'ngx-cookie-service'

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  constructor(private _http: HttpClient, private cookieService: CookieService) {
    this.cookieService.set(
      'EventspaceRest',
      'kFEaePvrIbPYkOUAMqtQ3UFIw/PLj1AOvJSTh6KZOj9nIpyBPhFQPr0HxvM1sgUQT8PFeRfM6wT7SBx6MOilNQ94IHgSiNCYYhwXbrcsyfFUCcLQI/Q9F9yhu9o2mZsZclBJ82ekj5BKCijlBciemq45ujTlDZo5m6wbXGnixc/mdIPP1BvflFlqVBMMb+ZcMHBMYWMW9uNS5Wokos2uCbTTnCRo8fAUxLzG9mPPWtuoZz/cUINEok2PRTrUXYUlZ3Z6WWWey5yqQf77nd9H6IYEH6RLA6IrcG6awTfM35z10GnHljl6pB4g0DfGTImf/8MQG2YCen48+OAsHxLFh8jMzE6uPUUpbnFzuUuKS9v50d/YzQSl25WqPSZ4yGcw3z6Ll42jfF1Cy67zBnNzkZwLjB2KtA9NjbogPy7ecrayQsC/tO6VWqnqrHhoiyBA38x21PtYP70hSFzCkidtK2lJB+DXj3HeewETehM7nb1voJAEVDTNT4ukZmlQw851Gk6d3WdMVAd7J474pEqTjmvx3LfKCEedd06zy8GcQJu8OKTOSy0142N3qmRyCQIr//HcxmukRYaeIL1I6AK5pGC3TboEUBXH+4n8astKYkGnJJ8LbwCAp2b1NN32fr5nycexQmYBqqkghhDxWRJH0rrmTwzVzlo36Ivk4O6UQTu0oGMrZx0xo1604ECU1XRYmygTrVS5y/l3LGChIbdxuZ3kMHW8S8KaOnhWchviM9P3yZitiUfqML+N0x7aBZFmf8jUAC5ldv1bjqqV3ybdJEN5hz4Vk1NENRVrThpcucG4NhsSb5yW00DjKu8yUdkhDfNr3GccbnWDRfe0tjzis9WO4E6/25C/GkzlT/WOXM68FB9LphOdQ09zhNmoOtH5iIonOKmHjne20czCbGL+UW29aXi3YwUUnmcT0dtZ6xDWNWye1qvgI9x63NMjzuupmHpXyKTn+c6To1woFqcZrNMm8I5FrsTMN6sXqQzoya0Ew3DnnDY5ZEfSf7Rn8VxGCb+esGtRhwH2TrEpGd+qd7DQYk73rCcqqD+qhlwJvTRWZQJ0ILYX3/MBk49LBPL7APUVDdti19h5ex0ZtfjEAlVw69qRUE1I+tw7DlKWTdqfO4WlkgC5CW4c4QlgtTn5QtIHU8MmWdBa8UCZzd7s9rKbo5oI/TPM8vROZUfkdOoifFcxmE7q6qzY02FYJSOC+ARf/bF5hTr/ExCfqjS4hhq5EmsmGWLp/aB2mySLrJL72JzVssm7PEkdcODu3Dcf'
    )
  }

  getSpaceDetails(spaceId: number) {
    try {
      return this._http
        .get(
          environment.api + '/book/space/' + spaceId,
          this.getRequestOptions()
        )
        .toPromise()
    } catch (error) {
      this.handleError(error)
    }
  }

  getFutureBookingDates(spaceId: number) {
    try {
      return this._http
        .get(
          environment.api + '/futureBookedDates/space/' + spaceId,
          this.getRequestOptions()
        )
        .toPromise()
    } catch (error) {
      this.handleError(error)
    }
  }

  getPaymentSummaryInfo(bookingRequest) {
    try {
      return this._http
        .post(
          environment.api + '/book/bookingCharge',
          JSON.stringify(bookingRequest),
          this.getRequestOptions()
        )
        .toPromise()
    } catch (error) {
      this.handleError(error)
    }
  }

  bookSpace(bookingRequest) {
    try {
      return this._http
        .post(
          environment.api + '/book/space',
          JSON.stringify(bookingRequest),
          this.getRequestOptions()
        )
        .toPromise()
    } catch (error) {
      this.handleError(error)
    }
  }

  verifyPromoCode(requestBody: any) {
    try {
      return this._http
        .post(
          environment.api + '/space/promo',
          JSON.stringify(requestBody),
          this.getRequestOptions()
        )
        .toPromise()
    } catch (error) {
      this.handleError(error)
    }
  }

  private getRequestOptions() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    })
    let options = {
      headers: headers,
      withCredentials: true
    }
    return options
  }

  handleError(error: any) {
    return Promise.reject(new Error(error))
  }
}
