import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { BookingState } from '../state/booking.reducer';
import { BookingService } from 'src/app/booking.service';
import * as fromBooking from '../state/booking.reducer';
@Component({
  selector: 'app-payment-summery',
  templateUrl: './payment-summery.component.html',
  styleUrls: ['./payment-summery.component.scss']
})
export class PaymentSummeryComponent implements OnInit {
  pageId = 3;
  paymentInfo;
  displayAccordian = false;
  spaceId: number;
  discountAmount = 0;
  validPromo = false;
  selectedGuestCount: number;
  blockChargeType: number;
  amenityCost: number;
  showBalancePayable = false;
  _advancePaymentEnabled = false;

  @ViewChild('promoInput') promoInput: ElementRef;

  constructor(
    private store: Store<BookingState>,
    private bookingService: BookingService
  ) {}

  ngOnInit() {
    this.store.dispatch({
      type: 'SET_PAGE_ID',
      payload: this.pageId
    });

    this.store
      .pipe(select(fromBooking.getSpaceDetails))
      .subscribe(spaceDetails => {
        if (spaceDetails) {
          this.spaceId = spaceDetails.id;
          this.blockChargeType = spaceDetails.blockChargeType;
        }
      });

    this.store
      .pipe(select(fromBooking.getPaymentInfo))
      .subscribe(paymentInfo => {
        this.paymentInfo = paymentInfo;
      });

    this.store
      .pipe(select(fromBooking.getSelectedGuestCount))
      .subscribe(selectedGuestCount => {
        this.selectedGuestCount = selectedGuestCount;
      });

    this.store
      .pipe(select(fromBooking.getAmenityCost))
      .subscribe(amenityCost => {
        this.amenityCost = amenityCost;
      });

    this.store
      .pipe(select(fromBooking.getDiscountAmount))
      .subscribe(discountAmount => {
        this.discountAmount = discountAmount;
      });
  }

  get totalCost() {
    return this.paymentInfo.bookingCharge - this.discountAmount;
  }

  toggleAccordian() {
    this.displayAccordian = !this.displayAccordian;
  }

  applyPromoCode() {
    this.store.dispatch({
      type: 'SET_PROMO_MODAL',
      payload: true
    });
  }

  async verifyPromo(form) {
    if (!form.promoCode) {
      return;
    }

    try {
      // reset promo input
      this.promoInput.nativeElement.value = '';

      // reset discount state to 0 each and every submit
      this.store.dispatch({
        type: 'SET_DISCOUNT_AMOUNT',
        payload: 0
      });

      const payload = {
        space: this.spaceId,
        promoCode: form.promoCode
      };

      const response: any = await this.bookingService.verifyPromoCode(payload);

      if (response.status === 1) {
        // valid promo
        this.validPromo = true;
        const flatDiscount = response.promoDetails.isFlatDiscount;
        let discountAmount = 0;

        if (flatDiscount === 0) {
          discountAmount = Math.floor(
            this.paymentInfo.bookingCharge *
              (response.promoDetails.discount / 100)
          );
        } else if (flatDiscount === 1) {
          discountAmount = this.getFlatDiscountAmount(
            response.promoDetails.discount
          );
        }
        this.store.dispatch({
          type: 'SET_DISCOUNT_AMOUNT',
          payload: discountAmount
        });
        this.store.dispatch({
          type: 'SET_PROMO_INFO',
          payload: {
            promoCode: form.promoCode,
            isValid: true,
            message: 'Promo code successfully redeemed!'
          }
        });
      } else {
        // not a valid promo
        this.store.dispatch({
          type: 'SET_DISCOUNT_AMOUNT',
          payload: 0
        });
        this.store.dispatch({
          type: 'SET_PROMO_INFO',
          payload: {
            promoCode: form.promoCode,
            isValid: false,
            message: 'Invalid promo code!'
          }
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  getFlatDiscountAmount(discountAmount: number) {
    let calAmount = 0;
    let totalAmount = this.paymentInfo.totalCharge;

    if (totalAmount < discountAmount) {
      calAmount = totalAmount;
    } else {
      calAmount = discountAmount;
    }
    return calAmount;
  }

  set advancePaymentEnabled(value) {
    this._advancePaymentEnabled = value;
    this.store.dispatch({
      type: 'SET_ADVANCE_ENABLED',
      payload: value
    });
  }

  get advancePaymentEnabled() {
    return this._advancePaymentEnabled;
  }
}
