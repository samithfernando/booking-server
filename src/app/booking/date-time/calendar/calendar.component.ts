import {
  Component,
  AfterViewInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';

declare const $;
declare const clndr;
declare const moment;

@Component({
  selector: 'app-calendar',
  template: `
    <div class="book-cal"></div>
  `,
  styles: [
    `
      :host >>> .clndr-controls {
        font-size: 20px;
      }
      :host >>> .clndr-controls {
        font-size: 14px;
        margin-bottom: 20px;
      }
      :host >>> .clndr-control-button,
      :host >>> .clndr-control-button {
        display: inline-block;
        width: 22%;
      }
      :host >>> .month {
        display: inline-block;
        width: 52%;
      }
      /*:host >>> .month {*/
      /*text-align: center;*/
      /*}*/
      /*:host >>> .clndr-next-btn-wrapper {*/
      /*text-align: right;*/
      /*}*/
      :host >>> .header-days {
        /*display: inline-block;*/
      }
      :host >>> .header-days > div {
        display: inline-block;
        width: 14.2%;
      }
      :host >>> .clndr-weeks-wrapper > div {
      }
      :host >>> .clndr-weeks-wrapper > div > div {
        display: inline-block;
        width: 14.2%;
        height: 50px;
      }
      :host >>> .book-cal {
        text-align: center;
        background: #ffffff;
      }
      :host >>> .day-contents {
        display: table;
        margin: 0 auto;
        height: 100%;
      }
      :host >>> .day-cell {
        display: table-cell;
        vertical-align: middle;
        height: 100%;
        position: relative;
        width: 15px;
      }
      :host >>> .adjacent-month {
        color: #e2e2e2;
      }
      :host >>> .day-contents {
        cursor: pointer;
      }
      :host >>> .month {
        font-size: 16px;
      }
      :host >>> .clndr-next-button,
      :host >>> .clndr-previous-button {
        cursor: pointer;
        font-size: 22px;
        padding: 10px 20px;
      }
      :host >>> .event > div > div > span {
        position: absolute;
        background: #388fff;
        bottom: 7px;
        left: 7px;
        width: 5px;
        height: 5px;
        display: inline-block;
        border-radius: 50%;
      }
      :host >>> .today {
        border-radius: 50%;
        color: #388fff;
        width: 49px;
        height: 40px;
        font-weight: bold;
      }

      /*:host >>> .today.event > div > div > span {*/
      /*left: 23px;*/
      /*}*/

      :host >>> .inactive {
        color: #989898;
      }
      :host >>> .selected {
        background: #ffdd57;
        border-radius: 10px;
      }
      :host >>> .past {
        color: #989898;
        background: transparent;
      }
    `
  ]
})
export class CalendarComponent implements AfterViewInit, OnChanges {
  @Input() spaceDetails;
  @Input() calendarEvents;
  @Output() selectedDate: EventEmitter<any> = new EventEmitter();

  bookingCalendar: any;

  clndrTemplate = `
  <div class='clndr-controls'>
      <div class='clndr-control-button'>
          <span class='clndr-previous-button'><i class="fa fa-angle-left" aria-hidden="true"></i></span>
      </div
      ><div class='month'><%= month %> <%= year %></div
      ><div class='clndr-control-button clndr-next-btn-wrapper'>
          <span class='clndr-next-button'><i class="fa fa-angle-right" aria-hidden="true"></i></span>
      </div>
  </div>
  <div>
      <div>
          <div class="header-days"><% for(var i = 0; i < daysOfTheWeek.length; i++) { %><div class='header-day'><%= daysOfTheWeek[i] %></div><% } %></div>
      </div>
      <div class="clndr-weeks-wrapper"><% for(var i = 0; i < numberOfRows; i++){ %><div><% for(var j = 0; j < 7; j++){ %><% var d = j + i * 7; %><div class='<%= days[d].classes %>'><div class='day-contents'><div class="day-cell"><%= days[d].day %><span></span></div></div></div><% } %></div><% } %></div>
  </div>
`;

  constructor() {}

  ngAfterViewInit() {
    this.bookingCalendar = (<any>$('.book-cal')).clndr({
      constraints: {
        endDate: this.spaceDetails ? this.spaceDetails.calendarEnd : null,
        startDate: moment().format('YYYY-MM-DD')
      },
      events: this.calendarEvents,
      template: this.clndrTemplate,
      trackSelectedDate: true,
      daysOfTheWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
      weekOffset: 1,
      ignoreInactiveDaysInSelection: true,
      showAdjacentMonths: true,
      adjacentDaysChangeMonth: false,
      classes: {
        past: 'past',
        today: 'today',
        event: 'event',
        inactive: 'inactive',
        selected: 'selected',
        lastMonth: 'last-month',
        nextMonth: 'next-month',
        adjacentMonth: 'adjacent-month'
      },
      clickEvents: {
        click: target => {
          // ignore event from inactive cells
          if (
            $(target.element).hasClass('past') ||
            $(target.element).hasClass('inactive')
          ) {
            return;
          }
          this.selectedDate.emit({
            day: target.date.weekday(),
            date: moment(target.date).format('YYYY-MM-DD')
          });
        }
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['calendarEvents'] || !changes['calendarEvents'].currentValue) {
      return;
    }

    if (this.bookingCalendar) {
      this.bookingCalendar.setEvents(changes['calendarEvents'].currentValue);
    }
  }
}
