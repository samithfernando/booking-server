import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { BookingState } from '../../state/booking.reducer';

declare const moment;

@Component({
  selector: 'app-block-base',
  templateUrl: './block-base.component.html',
  styleUrls: ['./block-base.component.scss']
})
export class BlockBaseComponent implements OnChanges {
  @Input() spaceDetails;
  @Input() bookingDates;
  _selectedDate;

  displayBlockCount = 5;
  dayAvailabilityBlocks: any[] = [];
  blocksLoading = false;

  toggleLoadMoreBlock = false;
  spaceFutureBookingDates: any[] = [];

  constructor(private store: Store<BookingState>) {}

  @Input()
  set selectedDate(date) {
    if (!date) {
      return;
    }
    this._selectedDate = date;
    // clear existing day blocks and set new blocks for a new day
    this.blocksLoading = true;
    this.dayAvailabilityBlocks = [];

    setTimeout(() => {
      this.setDayAvailability(date);
      this.blocksLoading = false;
    }, 200);
  }

  ngOnChanges(changes: SimpleChanges) {
    // const bookingDates = changes['bookingDates'];
    // if (!bookingDates || !bookingDates.currentValue) {
    //   return;
    // }
    if (this.bookingDates && this.bookingDates.length > 0) {
      const blockTimesForPeriod = [];
      this.bookingDates.forEach(date => {
        blockTimesForPeriod.push(
          this.getBlockTimeRange(
            moment(date.from).format('YYYY-MM-DDTHH:00'),
            moment(date.to).format('YYYY-MM-DDTHH:00')
          )
        );
        // merge blockTimesForPeriod
        this.spaceFutureBookingDates = blockTimesForPeriod
          .reduce((a, b) => a.concat(b), [])
          .reduce((a, b) => a.concat(b), []);
      });
    }
  }

  setDayAvailability(selectedDate: any) {
    let chosenDay = 0;

    switch (selectedDate.day) {
      case 1:
        chosenDay = 8;
        break;
      case 2:
        chosenDay = 8;
        break;
      case 3:
        chosenDay = 8;
        break;
      case 4:
        chosenDay = 8;
        break;
      case 5:
        chosenDay = 8;
        break;
      case 6:
        chosenDay = 6;
        break;
      case 0:
        chosenDay = 7;
        break;
    }

    const dayAvailabilityArray = this.spaceDetails.availability.filter(
      availability => availability.day === chosenDay
    );

    // create day availability
    dayAvailabilityArray.forEach(dayAvailability => {
      this.dayAvailabilityBlocks.push({
        date: selectedDate.date,
        // if it is a week day value should be 8
        day:
          dayAvailability.day !== 6 && dayAvailability.day !== 7
            ? 8
            : dayAvailability.day,
        charge: +dayAvailability.charge,
        from: +dayAvailability.from.split(':')[0],
        to: +dayAvailability.to.split(':')[0],
        blocked: false,
        chosen: false,
        tempBlock: false,
        tempBlockStatus: []
      });
    });

    this.tempBlockSameDayPast();

    // disabled matching blocks based on future booking dates
    if (this.spaceFutureBookingDates.length > 0) {
      this.spaceFutureBookingDates.forEach(bookedDate => {
        if (bookedDate.date === selectedDate.date) {
          this.dayAvailabilityBlocks.forEach(dayAvailabilityBlock => {
            if (
              dayAvailabilityBlock.from >= bookedDate.from &&
              dayAvailabilityBlock.to <= bookedDate.to
            ) {
              dayAvailabilityBlock.blocked = true;
            } else if (
              dayAvailabilityBlock.to > bookedDate.from &&
              dayAvailabilityBlock.from < bookedDate.to
            ) {
              dayAvailabilityBlock.blocked = true;
            }
          });
        }
      });
    }
    console.log('this.dayAvailabilityBlocks', this.dayAvailabilityBlocks);

    this.store.dispatch({
      type: 'SET_DAY_AVAILABILITY_BLOCKS',
      payload: this.dayAvailabilityBlocks
    });
    // this.bookingService.dayAvailabilityBlocks = this.dayAvailabilityBlocks;
  }

  tempBlockSameDayPast() {
    this.dayAvailabilityBlocks.forEach(block => {
      if (
        moment(block.date).format('YYYY-MM-DD') ===
        moment(new Date()).format('YYYY-MM-DD')
      ) {
        const hours = new Date().getHours();
        if (hours >= block.from || hours >= block.to) {
          block.blocked = true;
        }
      }
    });
  }

  toggleLoadMore() {
    if (this.toggleLoadMoreBlock) {
      this.toggleLoadMoreBlock = false;
      this.displayBlockCount = 5;
    } else {
      this.toggleLoadMoreBlock = true;
      this.displayBlockCount = this.dayAvailabilityBlocks.length;
    }
  }

  toggleBlock(selectedBlock) {
    this.dayAvailabilityBlocks.forEach(block => {
      if (block.from === selectedBlock.from && block.to === selectedBlock.to) {
        block.chosen = !block.chosen;
      }
    });

    // find chosen blocks
    const chosenBlocks = this.dayAvailabilityBlocks.filter(
      block => block.chosen
    );

    // if chosen blocks is 0 then disable temporary blocks
    if (chosenBlocks.length === 0) {
      this.dayAvailabilityBlocks.forEach(block => {
        block.tempBlock = false;
        block.tempBlockStatus = [];
      });
    } else {
      for (const avlBlock of this.dayAvailabilityBlocks) {
        const tempBlockArray = [];
        for (const chosenBlock of chosenBlocks) {
          // avoid same time checking
          if (
            avlBlock.from === chosenBlock.from &&
            avlBlock.to === chosenBlock.to
          ) {
            continue;
          }

          // find overlaping blocks based on chosen time periods
          if (
            (chosenBlock.from <= avlBlock.from &&
              avlBlock.from < chosenBlock.to) ||
            (chosenBlock.from < avlBlock.to && avlBlock.to <= chosenBlock.to) ||
            (avlBlock.from <= chosenBlock.from && chosenBlock.to <= avlBlock.to)
          ) {
            tempBlockArray.push(true);
          } else {
            tempBlockArray.push(false);
          }
        }
        avlBlock.tempBlockStatus = tempBlockArray;
      }

      // console.info('dayAvailabilityBlocks', this.dayAvailabilityBlocks);
      for (const block of this.dayAvailabilityBlocks) {
        block.tempBlock = block.tempBlockStatus.some(status => status === true);
      }
    }

    const total = chosenBlocks
      .map(block => block.charge)
      .reduce((t, charge) => t + charge, 0);

    const numberOfHours = this.getNumberOfHours(chosenBlocks);
    console.log('total', total);

    this.store.dispatch({
      type: 'SET_TOTAL_COST',
      payload: total
    });

    this.store.dispatch({
      type: 'SET_NUMBER_OF_HOURS',
      payload: numberOfHours
    });

    // this.bookingService.dayAvailabilityBlocks = this.dayAvailabilityBlocks;
    // this.bookingService.numberOfHours = this.getNumberOfHours(chosenBlocks);
    // this.spaceCharge.emit(total);
  }

  getBlockTimeRange(fromDateTime: any, toDateTime: any) {
    // fromDateTime => 2018-09-26T14:00, toDateTime => 2018-09-27T23:00
    const fromDate = moment(fromDateTime).format('YYYY-MM-DD'); // 2018-09-26
    const endDate = moment(toDateTime).format('YYYY-MM-DD'); // 2018-09-27

    const fromHour = +moment(fromDateTime).format('HH'); // 14
    const endHour = +moment(toDateTime).format('HH'); // 23

    const blockTimes = [];

    // same day
    if (fromDate === endDate) {
      blockTimes.push({
        date: fromDate,
        from: fromHour,
        to: endHour
      });
    }
    // not same day
    else {
      const allDates = this.getEnumerateDaysBetweenDates(fromDate, endDate);
      allDates[0].from = fromHour;
      allDates[allDates.length - 1].to = endHour;
      blockTimes.push(allDates);
    }

    return blockTimes;
  }

  // returns an array of dates between the two dates
  getEnumerateDaysBetweenDates(startDate, endDate) {
    startDate = moment(startDate);
    endDate = moment(endDate);

    const now = startDate,
      dates = [];

    while (now.isBefore(endDate) || now.isSame(endDate)) {
      dates.push({ date: now.format('YYYY-MM-DD'), from: 0, to: 24 });
      now.add(1, 'days');
    }
    return dates;
  }

  getCalendarFulldayBlocks(futureBookingDates: Array<any>) {
    console.log('futureBookingDates', futureBookingDates);
    return futureBookingDates.map(futureBooking => ({
      date: futureBooking.date
    }));
  }

  getNumberOfHours(chosenBlocks: any) {
    return chosenBlocks
      .map(block => block.to - block.from)
      .reduce((total, charge) => total + charge, 0);
  }
}
