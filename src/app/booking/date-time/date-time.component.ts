import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core'
import { Store, select } from '@ngrx/store'
import { BookingState } from '../state/booking.reducer'
import * as fromBooking from '../state/booking.reducer'

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.component.html',
  styleUrls: ['./date-time.component.scss']
})
export class DateTimeComponent implements OnInit, OnChanges {
  pageId = 1
  fullBookedDates = []
  calendarEnd
  selectedDate

  spaceDetails
  calendarEvents
  bookingDates

  constructor(private store: Store<BookingState>) {}

  ngOnInit() {
    this.store.dispatch({
      type: 'SET_PAGE_ID',
      payload: this.pageId
    })

    this.store
      .pipe(select(fromBooking.getSpaceDetails))
      .subscribe(spaceDetails => {
        this.spaceDetails = spaceDetails
      })

    this.store
      .pipe(select(fromBooking.getCalendarEvents))
      .subscribe(calendarEvents => {
        this.calendarEvents = calendarEvents
      })

    this.store
      .pipe(select(fromBooking.getFutureBookingDates))
      .subscribe(bookingDates => {
        this.bookingDates = bookingDates
      })
  }

  setSelectedDate(selectedDate) {
    this.selectedDate = selectedDate
    this.store.dispatch({
      type: 'SET_SELECTED_DATE',
      payload: selectedDate
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
  }
}
