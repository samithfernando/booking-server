import { createFeatureSelector, createSelector } from '@ngrx/store';

type promoInfo = { promoCode: string; isValid: boolean; message: string };
export interface BookingState {
  selectedPageId: number;
  spaceDetails: any;
  totalCost: number;
  amenityCost: number;
  numberOfHours: number;
  dayAvailabilityBlocks: any[];
  selectedDate: any;
  paymantInfo: any;
  calendarEvents: any[];
  futureBookingDates: any[];
  selectedSeating: number;
  selectedEventType: number;
  selectedGuestCount: number;
  extraAmenities: any[];
  promoInfo: promoInfo;
  discountAmount: number;
  advancedEnabled: boolean;
}

const initialState: BookingState = {
  selectedPageId: 1,
  spaceDetails: null,
  totalCost: 0,
  amenityCost: 0,
  numberOfHours: 0,
  dayAvailabilityBlocks: [],
  selectedDate: null,
  paymantInfo: null,
  calendarEvents: [],
  futureBookingDates: [],
  selectedSeating: 99,
  selectedEventType: 99,
  selectedGuestCount: 1,
  extraAmenities: [],
  promoInfo: null,
  discountAmount: 0,
  advancedEnabled: false
};

const getBookingFeatureState = createFeatureSelector<BookingState>('bookings');

export const getSelectedPageId = createSelector(
  getBookingFeatureState,
  state => state.selectedPageId
);

export const getSpaceDetails = createSelector(
  getBookingFeatureState,
  state => state.spaceDetails
);

export const getTotalCost = createSelector(
  getBookingFeatureState,
  state => state.totalCost
);

export const getAmenityCost = createSelector(
  getBookingFeatureState,
  state => state.amenityCost
);

export const getNumberOfHours = createSelector(
  getBookingFeatureState,
  state => state.numberOfHours
);

export const getDayAvailabilityBlocks = createSelector(
  getBookingFeatureState,
  state => state.dayAvailabilityBlocks
);

export const getSelectedDate = createSelector(
  getBookingFeatureState,
  state => state.selectedDate
);

export const getPaymentInfo = createSelector(
  getBookingFeatureState,
  state => state.paymantInfo
);

export const getCalendarEvents = createSelector(
  getBookingFeatureState,
  state => state.calendarEvents
);

export const getFutureBookingDates = createSelector(
  getBookingFeatureState,
  state => state.futureBookingDates
);

export const getSelectedEventType = createSelector(
  getBookingFeatureState,
  state => state.selectedEventType
);

export const getSelectedSeating = createSelector(
  getBookingFeatureState,
  state => state.selectedSeating
);

export const getSelectedGuestCount = createSelector(
  getBookingFeatureState,
  state => state.selectedGuestCount
);

export const getExtraAmenities = createSelector(
  getBookingFeatureState,
  state => state.extraAmenities
);

export const getDiscountAmount = createSelector(
  getBookingFeatureState,
  state => state.discountAmount
);

export const getPromoInfo = createSelector(
  getBookingFeatureState,
  state => state.promoInfo
);

export const getAdvancedEnabled = createSelector(
  getBookingFeatureState,
  state => state.advancedEnabled
);

export function reducer(state = initialState, action): BookingState {
  switch (action.type) {
    case 'SET_PAGE_ID':
      return {
        ...state,
        selectedPageId: action.payload
      };

    case 'SET_SPACE_DETAILS':
      return {
        ...state,
        spaceDetails: action.payload
      };

    case 'SET_TOTAL_COST':
      return {
        ...state,
        totalCost: action.payload
      };

    case 'SET_AMENITY_COST':
      return {
        ...state,
        amenityCost: action.payload
      };

    case 'SET_NUMBER_OF_HOURS':
      return {
        ...state,
        numberOfHours: action.payload
      };

    case 'SET_DAY_AVAILABILITY_BLOCKS':
      return {
        ...state,
        dayAvailabilityBlocks: action.payload
      };

    case 'SET_SELECTED_DATE':
      return {
        ...state,
        selectedDate: action.payload
      };

    case 'SET_PAYMENT_INFO':
      return {
        ...state,
        paymantInfo: action.payload
      };

    case 'SET_CALENDAR_EVENTS':
      return {
        ...state,
        calendarEvents: action.payload
      };

    case 'SET_FUTURE_BOOKING_DATES':
      return {
        ...state,
        futureBookingDates: action.payload
      };

    case 'SET_SELECTED_EVENT_TYPE':
      return {
        ...state,
        selectedEventType: action.payload
      };

    case 'SET_SELECTED_SEATING':
      return {
        ...state,
        selectedSeating: action.payload
      };

    case 'SET_SELECTED_GUEST_COUNT':
      return {
        ...state,
        selectedGuestCount: action.payload
      };

    case 'SET_EXTRA_AMENITIES':
      return {
        ...state,
        extraAmenities: action.payload
      };

    case 'SET_DISCOUNT_AMOUNT':
      return {
        ...state,
        discountAmount: action.payload
      };

    case 'SET_PROMO_INFO':
      return {
        ...state,
        promoInfo: action.payload
      };

    case 'SET_ADVANCE_ENABLED':
      return {
        ...state,
        advancedEnabled: action.payload
      };

    default:
      return state;
  }
}

//
