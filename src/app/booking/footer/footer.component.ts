import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { BookingState } from '../state/booking.reducer';
import * as fromBooking from '../state/booking.reducer';
import { Router, ActivatedRoute } from '@angular/router';
import { BookingService } from 'src/app/booking.service';
import io from 'socket.io-client';
import Notification from '../../shared/models/notification';

declare const moment: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  userId: number;
  totalCost: number = 0;
  amenityCost: number = 0;
  spaceName: string;
  address: string;
  organizationName: string;
  selectedEventType: number;
  selectedSeating: number;
  blockChargeType: number;
  selectedGuestCount: number;
  spaceId: number;
  advanceOnlyEnable: number;
  dayAvailabilityBlocks: any[] = [];
  selectedDate: any;
  numberOfHoursSelected: number;
  extraAmenities: any[] = [];
  paymentInfo;

  numberOfHours: number = 0;
  pageId: number;

  promptConatctModal: boolean = false;
  promoInfo: any;
  discountAmount: number = 0;
  modalActive: boolean = false;
  advancedEnabled = false;

  socket: any;

  constructor(
    private store: Store<BookingState>,
    private router: Router,
    private route: ActivatedRoute,
    private bookingService: BookingService
  ) {}

  ngOnInit() {
    this.socket = io('https://qnserver.millionspaces.com');

    this.store.pipe(select('bookings')).subscribe(bookings => {
      this.totalCost = +bookings.totalCost;
      this.amenityCost = +bookings.amenityCost;
      if (bookings.spaceDetails) {
        this.spaceId = bookings.spaceDetails.id;
        this.spaceName = bookings.spaceDetails.name;
        this.organizationName = bookings.spaceDetails.organizationName;
        this.address = bookings.spaceDetails.addressLine1;
        this.blockChargeType = bookings.spaceDetails.blockChargeType;
        this.advanceOnlyEnable = bookings.advanceOnlyEnable;
        this.userId = bookings.spaceDetails.userDro.id;
        // this.socket.emit('intiate', bookings.spaceDetails.userDro.id);
      }
    });

    this.store
      .pipe(select(fromBooking.getNumberOfHours))
      .subscribe(numberOfHours => {
        this.numberOfHours = numberOfHours;
      });

    this.store.pipe(select(fromBooking.getSelectedPageId)).subscribe(pageId => {
      this.pageId = pageId;
    });

    this.store
      .pipe(select(fromBooking.getSelectedEventType))
      .subscribe(selectedEventType => {
        this.selectedEventType = selectedEventType;
      });

    this.store
      .pipe(select(fromBooking.getSelectedSeating))
      .subscribe(selectedSeating => {
        this.selectedSeating = selectedSeating;
      });

    this.store
      .pipe(select(fromBooking.getSelectedGuestCount))
      .subscribe(selectedGuestCount => {
        this.selectedGuestCount = selectedGuestCount;
      });

    this.store
      .pipe(select(fromBooking.getDayAvailabilityBlocks))
      .subscribe(dayAvailabilityBlocks => {
        this.dayAvailabilityBlocks = dayAvailabilityBlocks;
      });

    this.store
      .pipe(select(fromBooking.getSelectedDate))
      .subscribe(selectedDate => {
        this.selectedDate = selectedDate;
      });

    this.store
      .pipe(select(fromBooking.getNumberOfHours))
      .subscribe(numberOfHours => {
        this.numberOfHoursSelected = numberOfHours;
      });

    this.store
      .pipe(select(fromBooking.getExtraAmenities))
      .subscribe(extraAmenities => {
        this.extraAmenities = extraAmenities;
      });

    this.store
      .pipe(select(fromBooking.getDiscountAmount))
      .subscribe(discountAmount => {
        this.discountAmount = discountAmount;
      });

    this.store
      .pipe(select(fromBooking.getAmenityCost))
      .subscribe(amenityCost => {
        this.amenityCost = amenityCost;
      });

    this.store.pipe(select(fromBooking.getPromoInfo)).subscribe(promoInfo => {
      this.promoInfo = promoInfo;
      this.modalActive = true;
    });

    this.store
      .pipe(select(fromBooking.getPaymentInfo))
      .subscribe(paymentInfo => {
        this.paymentInfo = paymentInfo;
      });

    this.store
      .pipe(select(fromBooking.getAdvancedEnabled))
      .subscribe(advancedEnabled => {
        this.advancedEnabled = advancedEnabled;
      });
  }

  async continueBooking() {
    if (this.pageId === 1) {
      this.router.navigate(['page2'], { relativeTo: this.route });
    } else if (this.pageId === 2) {
      if (
        !this.selectedGuestCount ||
        (this.selectedEventType === 99 || this.selectedSeating === 99)
      ) {
        alert('error');
      } else {
        const paymentInfo = await this.continueToPaymentSummery();
        this.store.dispatch({
          type: 'SET_PAYMENT_INFO',
          payload: paymentInfo
        });
        this.router.navigate(['page3'], { relativeTo: this.route });
      }
    } else if (this.pageId === 3) {
      // this.promptConatctModal = true
      this.proceedToPayments();
    }
  }

  get totalBookingCharge() {
    return (
      (this.blockChargeType === 2
        ? this.selectedGuestCount * this.totalCost
        : this.totalCost) +
      this.amenityCost -
      this.discountAmount
    );
  }

  continueToPaymentSummery() {
    try {
      const bookingRequest = {
        space: this.spaceId,
        eventType: this.selectedEventType,
        guestCount: this.selectedGuestCount,
        advanceOnly: this.advanceOnlyEnable === 1 ? true : false,
        dates: this.getSelectedTimePeriod(),
        bookingWithExtraAmenityDtoSet: this.getSelectedAmenityUnits()
      };
      return this.bookingService.getPaymentSummaryInfo(bookingRequest);
    } catch (error) {
      console.error(error);
    }
  }

  getSelectedTimePeriod() {
    const chosenBlocks = this.dayAvailabilityBlocks
      .filter(block => block.chosen)
      .map(block => {
        return {
          fromDate: this.selectedDate.date + 'T' + block.from + ':' + '00Z',
          toDate: this.selectedDate.date + 'T' + block.to + ':' + '00Z'
        };
      });

    return chosenBlocks;
  }

  getSelectedAmenityUnits() {
    const selectedPerHourAmenities = this.extraAmenities
      .filter(amenity => amenity.amenityUnit === 1 && amenity.selected)
      .map(amenity => {
        return {
          amenityId: amenity.amenityId,
          number: this.numberOfHoursSelected
        };
      });

    const selectedOtherAmenities = this.extraAmenities
      .filter(amenity => amenity.amenityUnit !== 1 && amenity.count > 0)
      .map(amenity => {
        return {
          amenityId: amenity.amenityId,
          number: amenity.count
        };
      });

    return selectedPerHourAmenities.concat(selectedOtherAmenities);
  }

  async proceedToPayments() {
    try {
      const bookingRequest = {
        space: this.spaceId,
        eventType: this.selectedEventType,
        promoCode: null,
        guestCount: this.selectedGuestCount,
        bookingCharge: this.totalBookingCharge,
        seatingArrangement: this.selectedSeating,
        dates: this.getSelectedTimePeriod(),
        bookingWithExtraAmenityDtoSet: this.getSelectedAmenityUnits(),
        advanceOnly: false
      };

      console.log('bookingRequest', bookingRequest);
      const bookingResponse = await this.bookingService.bookSpace(
        bookingRequest
      );

      const spaceDetails = {
        space: this.spaceName,
        organization: this.organizationName,
        address: this.address,
        date: moment(new Date(this.selectedDate.date)).format(
          'dddd Do MMMM YYYY'
        ),
        reservationTime: this.getReservationTime(),
        total: (this.advancedEnabled
          ? this.paymentInfo.payableAdvance
          : this.totalBookingCharge
        ).toFixed(2)
      };

      const toNotify: Notification = {
        topic: 'BOOKING_RESPONSE',
        to: this.userId,
        message: {
          bookingResponse,
          spaceDetails
        }
      };
      this.socket.emit('notify', toNotify);

      // console.log('bookingResponse', bookingResponse)
    } catch (error) {
      console.error('error', error);
    }
  }

  getReservationTime() {
    return this.dayAvailabilityBlocks
      .filter(block => block.chosen)
      .map(
        block =>
          this.get12HourTime(+block.from) + '-' + this.get12HourTime(+block.to)
      );
  }

  get12HourTime(value) {
    if (value === 12) return '12 PM';
    return value >= 12 ? value - 12 + ' PM' : value + ' AM';
  }
}
