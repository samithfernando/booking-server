import { Component, OnInit } from '@angular/core'
import { Store, select } from '@ngrx/store'
import { BookingState } from '../state/booking.reducer'
import * as fromBooking from '../state/booking.reducer'
import { BookingService } from 'src/app/booking.service'

@Component({
  selector: 'app-additional-info',
  templateUrl: './additional-info.component.html',
  styleUrls: ['./additional-info.component.scss']
})
export class AdditionalInfoComponent implements OnInit {
  pageId = 2
  advanceOnlyEnable: number
  eventTypes: any[] = []
  seatingArrangements: any[] = []
  maxGuestCount: number
  minGuestCount: number
  extraAmenities: any[] = []
  numberOfHoursSelected: number = 0

  _selectedGuestCount: number
  _selectedSeating: number
  _selectedEvent: number

  displayAccordian: boolean = false
  amenityCost: number = 0

  constructor(
    private store: Store<BookingState>,
    private bookingService: BookingService
  ) {}

  ngOnInit() {
    this.store.dispatch({
      type: 'SET_PAGE_ID',
      payload: this.pageId
    })

    this.store
      .pipe(select(fromBooking.getSpaceDetails))
      .subscribe(spaceDetails => {
        if (spaceDetails) {
          this.eventTypes = spaceDetails.eventType
          this.seatingArrangements = spaceDetails.seatingArrangements
          this.maxGuestCount = spaceDetails.participantCount
          this.minGuestCount = spaceDetails.minParticipantCount
          this.extraAmenities = spaceDetails.extraAmenity
          this.advanceOnlyEnable = spaceDetails.advanceOnlyEnable

          // initialize count 0 of each ameninity
          this.extraAmenities.forEach(amenity => ((<any>amenity).count = 0))

          // set min guest count
          // this._selectedGuestCount = +this.minGuestCount
        }
      })

    this.store
      .pipe(select(fromBooking.getNumberOfHours))
      .subscribe(numberOfHours => {
        this.numberOfHoursSelected = numberOfHours
      })

    this.store
      .pipe(select(fromBooking.getAmenityCost))
      .subscribe(amenityCost => {
        this.amenityCost = amenityCost
      })
  }

  toggleAccordian() {
    this.displayAccordian = !this.displayAccordian
  }

  increaseAmenityCount(amenity) {
    console.info('amenity', amenity)
    amenity.count++

    // per person or per unit
    if (amenity.amenityUnit === 2 || amenity.amenityUnit === 3) {
      this.amenityCost += amenity.extraRate
    }

    this.store.dispatch({
      type: 'SET_AMENITY_COST',
      payload: this.amenityCost
    })

    this.store.dispatch({
      type: 'SET_EXTRA_AMENITIES',
      payload: this.extraAmenities
    })
  }

  decreaseAmenityCount(amenity) {
    if (amenity.count === 0) {
      return
    }
    amenity.count--
    // per person
    if (amenity.amenityUnit === 2 || amenity.amenityUnit === 3) {
      this.amenityCost -= amenity.extraRate
    }

    this.store.dispatch({
      type: 'SET_AMENITY_COST',
      payload: this.amenityCost
    })

    this.store.dispatch({
      type: 'SET_EXTRA_AMENITIES',
      payload: this.extraAmenities
    })
  }

  calculateAmenityCost(amenity) {
    if (amenity.amenityUnit === 1) {
      if (amenity.selected) {
        return amenity.extraRate * this.numberOfHoursSelected
      } else {
        return 0
      }
    }

    return amenity.count * amenity.extraRate
  }

  onChangeAmenity(amenity) {
    amenity.selected = amenity.selected || null

    if (amenity.selected) {
      amenity.selected = false
      const deductTotal = amenity.extraRate * this.numberOfHoursSelected
      this.store.dispatch({
        type: 'SET_AMENITY_COST',
        payload: this.amenityCost - deductTotal
      })
    } else {
      amenity.selected = true
      const addTotal = amenity.extraRate * this.numberOfHoursSelected
      this.store.dispatch({
        type: 'SET_AMENITY_COST',
        payload: this.amenityCost + addTotal
      })
    }

    this.store.dispatch({
      type: 'SET_EXTRA_AMENITIES',
      payload: this.extraAmenities
    })
  }

  get selectedGuestCount() {
    return this._selectedGuestCount
  }

  set selectedGuestCount(guestCount) {
    this._selectedGuestCount = +guestCount
    this.store.dispatch({
      type: 'SET_SELECTED_GUEST_COUNT',
      payload: +guestCount
    })
  }

  get selectedEvent() {
    return this._selectedEvent
  }

  set selectedEvent(eventId) {
    this._selectedEvent = +eventId
    this.store.dispatch({
      type: 'SET_SELECTED_EVENT_TYPE',
      payload: +eventId
    })
  }

  get selectedSeating() {
    return this._selectedSeating
  }

  set selectedSeating(seatingOptionId) {
    this._selectedSeating = +seatingOptionId
    this.selectSeating(+seatingOptionId)
    this.store.dispatch({
      type: 'SET_SELECTED_SEATING',
      payload: +seatingOptionId
    })
  }

  selectSeating(seatingOptionId) {
    const seating = this.seatingArrangements.find(
      option => option.id === +seatingOptionId
    )

    if (this.selectedGuestCount > seating.participantCount)
      this.selectedGuestCount = seating.participantCount

    this.maxGuestCount = seating.participantCount
  }
}
