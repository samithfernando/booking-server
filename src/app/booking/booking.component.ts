import { Component, OnInit, AfterViewInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { bulmaSteps, bulmaAccordion } from 'bulma-extensions'
import { BookingService } from '../booking.service'
import { Store, select } from '@ngrx/store'
import { BookingState } from './state/booking.reducer'
import * as fromBooking from './state/booking.reducer'

declare const moment

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit, AfterViewInit {
  spaceDetails
  calendarEvents
  futureBookingDates
  pageId = 1

  constructor(
    private activeRoute: ActivatedRoute,
    private bookingService: BookingService,
    private store: Store<BookingState>
  ) {}

  async ngOnInit() {
    const routeParams = this.activeRoute.snapshot.params
    console.log('routeParams.id', routeParams.id)

    const spaceDetails = await this.getSpaceDetails(+routeParams.id)
    const { availabilityMethod } = <any>spaceDetails
    const futureBookingDates = await this.getFutureBookingDates(+routeParams.id)
    const calendarEvents = this.getCalendarEventDays(
      futureBookingDates,
      availabilityMethod
    )

    this.spaceDetails = spaceDetails
    this.calendarEvents = calendarEvents
    this.futureBookingDates = futureBookingDates

    this.store.dispatch({
      type: 'SET_SPACE_DETAILS',
      payload: spaceDetails
    })

    this.store.dispatch({
      type: 'SET_CALENDAR_EVENTS',
      payload: calendarEvents
    })

    this.store.dispatch({
      type: 'SET_FUTURE_BOOKING_DATES',
      payload: futureBookingDates
    })

    this.store.pipe(select(fromBooking.getSelectedPageId)).subscribe(pageId => {
      this.pageId = pageId
    })
  }

  ngAfterViewInit() {
    bulmaSteps.attach()
    // bulmaAccordion.attach()

    function promoModal() {
      const promoId = document.getElementById('promoModal')
      promoId.classList.add('is-active')
    }
  }

  getSpaceDetails(spaceId: number) {
    return this.bookingService.getSpaceDetails(spaceId)
  }

  getCalendarEventDays(fbd, availabilityMethod) {
    const eventDaysCalendar = []

    if (availabilityMethod === 'HOUR_BASE') {
      fbd.forEach(date => {
        const startDate = moment(date.from).format('YYYY-MM-DD')
        const endDate = moment(date.to).format('YYYY-MM-DD')

        // same day booking
        if (startDate === endDate) {
          eventDaysCalendar.push({ date: startDate })
        } else {
          // not same day
          let dates = this.getDates(new Date(startDate), new Date(endDate))
          dates = dates.map(d => ({ date: moment(d).format('YYYY-MM-DD') }))
          eventDaysCalendar.push(...dates)
        }
      })
    } else if (availabilityMethod === 'BLOCK_BASE') {
      ;(<any>fbd).forEach(date => {
        const startDate = moment(date.from).format('YYYY-MM-DD')
        const endDate = moment(date.to).format('YYYY-MM-DD')
        const endDateTime = moment(date.to).format('HH:mm')

        // same day booking
        if (startDate === endDate || endDateTime === '00:00') {
          eventDaysCalendar.push({ date: startDate })
        } else {
          // not same day
          let dates = this.getDates(new Date(startDate), new Date(endDate))
          dates = dates.map(d => ({ date: moment(d).format('YYYY-MM-DD') }))
          eventDaysCalendar.push(...dates)
        }
      })
    }

    return eventDaysCalendar
  }

  getFutureBookingDates(spaceId: number) {
    return this.bookingService.getFutureBookingDates(spaceId)
  }

  getDates(startDate, stopDate) {
    const dateArray = new Array()
    let currentDate = startDate
    while (currentDate <= stopDate) {
      dateArray.push(currentDate)
      currentDate = this.addDays(1, currentDate)
    }
    return dateArray
  }

  addDays(days, currentDate) {
    const date = new Date(currentDate.valueOf())
    date.setDate(date.getDate() + days)
    return date
  }
}
