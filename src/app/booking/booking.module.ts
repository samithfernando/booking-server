import { NgModule } from '@angular/core'
import { BookingComponent } from './booking.component'
import { DateTimeComponent } from './date-time/date-time.component'
import { AdditionalInfoComponent } from './additional-info/additional-info.component'
import { PaymentSummeryComponent } from './payment-summery/payment-summery.component'
import { SharedModule } from '../shared/shared.module'
import { CalendarComponent } from './date-time/calendar/calendar.component'
import { FooterComponent } from './footer/footer.component'
import { BlockBaseComponent } from './date-time/block-base/block-base.component'
import { HourBaseComponent } from './date-time/hour-base/hour-base.component'
import { StoreModule } from '@ngrx/store'
import { reducer } from './state/booking.reducer'
import { RouterModule } from '@angular/router'

@NgModule({
  declarations: [
    BookingComponent,
    DateTimeComponent,
    AdditionalInfoComponent,
    PaymentSummeryComponent,
    CalendarComponent,
    FooterComponent,
    BlockBaseComponent,
    HourBaseComponent
  ],
  imports: [
    SharedModule,
    RouterModule,
    StoreModule.forFeature('bookings', reducer)
  ]
})
export class BookingModule {}
