import { BookingState } from '../booking/state/booking.reducer'

export interface State {
  payment: BookingState
}
