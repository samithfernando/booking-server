import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterBlocks'
})
export class FilterBlocksPipe implements PipeTransform {
  transform(items: any[], numberOfBlocks: string): any {
    if (items.length === 0) {
      return [];
    }

    return items.slice(0, +numberOfBlocks + 1);
  }
}
