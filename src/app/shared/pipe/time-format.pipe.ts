import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {
  transform(value: any): any {
    if (+value === 12) {
      return '12 PM';
    }
    return +value >= 12 ? value - 12 + ' PM' : value + ' AM';
  }
}
