import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { SortPipe } from './pipe/sort.pipe';
import { FilterBlocksPipe } from './pipe/filter-blocks.pipe';
import { TimeFormatPipe } from './pipe/time-format.pipe';

@NgModule({
  declarations: [SpinnerComponent, SortPipe, FilterBlocksPipe, TimeFormatPipe],
  imports: [CommonModule],
  exports: [
    CommonModule,
    FormsModule,
    SpinnerComponent,
    SortPipe,
    FilterBlocksPipe,
    TimeFormatPipe
  ]
})
export class SharedModule {}
