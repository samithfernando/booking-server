import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { BookingComponent } from './booking/booking.component'
import { DateTimeComponent } from './booking/date-time/date-time.component'
import { AdditionalInfoComponent } from './booking/additional-info/additional-info.component'
import { PaymentSummeryComponent } from './booking/payment-summery/payment-summery.component'

const routes: Routes = [
  {
    path: 'booking/:id',
    component: BookingComponent,
    children: [
      { path: '', redirectTo: 'page1', pathMatch: 'full' },
      { path: 'page1', component: DateTimeComponent },
      { path: 'page2', component: AdditionalInfoComponent },
      { path: 'page3', component: PaymentSummeryComponent }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
